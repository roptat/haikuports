SUMMARY="GMP - GNU Multiple Precision Arithmetic Library" 
DESCRIPTION="
The GNU Multiple Precision Arithmetic Library (GMP) is a free library for \
arbitrary-precision arithmetic, operating on signed integers, rational numbers, \
and floating point numbers. There are no practical limits to the precision \
except the ones implied by the available memory in the machine GMP runs on \
(operand dimension limit is 2^(32)-1 bits on 32-bit machines and 2^37 bits on \
64-bit machines). GMP has a rich set of functions, and the functions have a \
regular interface. The basic interface is for C but wrappers exist for other \
languages including Ada, C++, C#, OCaml, Perl, PHP, and Python.
"
HOMEPAGE="http://gmplib.org/" 
SRC_URI="ftp://ftp.gmplib.org/pub/gmp-5.1.1/gmp-5.1.1.tar.xz"
CHECKSUM_SHA256="4bd64d782fdeb61aeed45b434fca5246d66baa9de76f87cba30c38460d8834c2"
LICENSE="GNU GPL v3
	GNU LGPL v3"
COPYRIGHT="1991-2013 Free Software Foundation, Inc."
REVISION="1"
ARCHITECTURES="x86 x86_gcc2"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"
PROVIDES="gmp$secondaryArchSuffix = $portVersion compat >= 5.1
	lib:libgmp$secondaryArchSuffix = 10.1.1 compat >= 10
	"
REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	"
BUILD_PREREQUIRES="
	cmd:gcc$secondaryArchSuffix
	cmd:libtool
	cmd:autoconf
	cmd:automake
	cmd:make
	"

BUILD()
{
	libtoolize --force --copy --install
	aclocal
	autoconf
	automake --add-missing
	runConfigure ./configure 
	make $jobArgs
}

INSTALL()
{
	make install includedir=$includeDir includeexecdir=$includeDir
	
	# prepare devel/lib
	prepareInstalledDevelLibs libgmp

	# devel package
	packageEntries devel \
		$developDir
}

TEST()
{
	make check
}

# ----- devel package 

PROVIDES_devel="
	gmp${secondaryArchSuffix}_devel = $portVersion
	devel:libgmp$secondaryArchSuffix = 10.1.1 compat >= 0
	"
REQUIRES_devel="
	gmp$secondaryArchSuffix == $portVersion base
	"

